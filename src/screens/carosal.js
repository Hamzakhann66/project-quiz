import React from 'react';
import Carousel from 'nuka-carousel';
import slide1 from  './sliderImg/img-1.jpg';
import slide2 from  './sliderImg/img-2.jpg';
import slide3 from  './sliderImg/img-3.jpeg';
export default class Slider extends React.Component {
  render() {
    return (
      <Carousel>
        <img src={slide1}  />
        <img src={slide2}  />
        <img src={slide3}  />
      </Carousel>
    );
  }
}