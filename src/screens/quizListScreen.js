import React, { Component } from "react";
import Slider from './carosal'
import "./custom.css";
import js from './quizImages/jjs.jpg';
import html from './quizImages/html.png';
import css from './quizImages/css.jpg';
import react from './quizImages/react.jpg';
import mdb from './quizImages/mdb.png';
import Footer from './footer';
class QuizListScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      quizImg: [js , html , css ,react , mdb ]
    };
  }
  render() {
    const { quizList, listDisa } = this.props;
    console.log(listDisa);
    return (
      <div>
        <nav class="navbar navbar-expand-lg navbar-light bg-nav">
          <a  class="navbar-brand text-white" href="#">
        Dashboard
          </a>
          <button
            class="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarText"
            aria-controls="navbarText"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span class="navbar-toggler-icon" />
          </button>
          <div class="collapse navbar-collapse" id="navbarText">
            <ul class="navbar-nav mr-auto">
            
            </ul>
            <button
              class="btn btn btn-outline-success mt-3 mb-3"
              onClick={this.clear}
            >
              LOGOUT
            </button>
          </div>
        </nav>
        <Slider />
        <br />
        <br />
        <div className="container" >
            <div className="row" >
            {quizList.map((res, index) => {
          console.log(res);
          //   return (<div><h1>{res.name}</h1><button onClick={() => listDisa(index)}>Open</button><hr /></div>);
          return (
            <div className="col-sm col-md-4" >
            <div class="card mt-5">
                <img
                  class="card-img-top"
                  src={this.state.quizImg[index]}
                  alt="Card image cap"
                />
                <div class="card-body">
                  <h5 class="card-title">{res.name}</h5>
                  <p class="card-text">
                    Some quick example text to build on the card title and make
                    up the bulk of the card's content.
                  </p>
                  <button
                    class="btn btn-primary btn-block"
                    onClick={() => listDisa(index)}
                  >
                    OPEN
                  </button>
                </div>
              </div>
            </div>
          );
        })}
            </div>
        </div>
        <br/>
        <br/>
        <Footer/>
      </div>
    );
  }

  clear() {
    localStorage.removeItem("quizAppLogin");
    window.location.reload();
  }
}

export default QuizListScreen;
