import React, { Component } from "react";
class OpenQuiz extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const { openQuizPage, back, startFunc } = this.props;
    console.log(startFunc);
    return (
      // <h1></h1><h2>{openQuizPage.questions} Questions</h2><h3>{openQuizPage.time}</h3><h1>------</h1>
      // <hr />
      <div class="container gap">
        <div className="jumbotron">
          <h2>Description :</h2>
          <p>
            First Javascript quiz covering chapters 21 to 42 of Smarter way to
            Learn javascript book. Important Note: Where specifically noted, you
            can select more than one option as a correct answer. For example you
            can select a, b, and c as the correct answer.
          </p>
          <h5>Passing Score : 65</h5>
          <h5>Quiz Duration : 30 Minutes</h5>
          <h5>No. of Attempts Allowed : 1</h5>
        </div>
        <ul class="list-group mt-5">
          <li class="list-group-item active">{openQuizPage.name}</li>
          {openQuizPage.list.map((value, index) => {
            return (
              <li class="list-group-item" onClick={() => startFunc(index)}>
                QUIZ {index + 1}
              </li>
            );
          })}
        </ul>
        <button class="btn btn-primary mt-3" onClick={back}>
          Back to list
        </button>
      </div>
    );
  }

  start(index) {
    const { startFunc } = this.props;
    console.log("first/*****" + index);
    startFunc(index);
  }
}

export default OpenQuiz;
